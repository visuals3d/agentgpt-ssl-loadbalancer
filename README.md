# agentgpt-ssl-loadbalancer

- Go into install.sh and edit the variables at the top to represent your setup
- Don't forget to let the desired domain point with its DNS A entries to to the ip of your server
- Running the install.sh script should setup everything. It is completely untested so it possible has some issues

```shell
git clone https://gitlab.com/visuals3d/agentgpt-ssl-loadbalancer.git
cd agentgpt-ssl-loadbalancer
chmod +x install.sh
./install.sh

```

