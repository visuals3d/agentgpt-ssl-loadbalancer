#!/bin/bash

MAIL_ADRESS="<YOUR MAIL>"
DOMAIN="example.com"
ABSOLUTE_APP_PATH="/root/AgentGPT"

# Install Cerbot
snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot

# Create Renewal hooks
cp ./templates/compose-pre /etc/letsencrypt/renewal-hooks/pre/compose-pre
sed -i "s/-PATH-/$ABSOLUTE_APP_PATH/" /etc/letsencrypt/renewal-hooks/pre/compose-pre

cp ./templates/compose-post /etc/letsencrypt/renewal-hooks/post/compose-post
sed -i "s/-PATH-/$ABSOLUTE_APP_PATH/" $filename

# Create Certs
certbot certonly --standalone --agree-tos --non-interactive -m $MAIL_ADRESS --cert-name production_certs -d www.$DOMAIN,$DOMAIN,www.api.$DOMAIN,api.$DOMAIN

# copy nginx config to app path
cp ./nginx.conf $ABSOLUTE_APP_PATH/nginx.conf

# Add Domain to nginx config
sed -i "s/-DOMAIN-/$DOMAIN/" $ABSOLUTE_APP_PATH/nginx.conf

# Replace localhost with domain
sed -i "s#http://localhost:8000#https://api.$DOMAIN#" $ABSOLUTE_APP_PATH/.env
sed -i "s#http://localhost:3000#https://$DOMAIN#" $ABSOLUTE_APP_PATH/.env
sed -i "s#http://localhost:8000#https://api.$DOMAIN#" $ABSOLUTE_APP_PATH/next/.env
sed -i "s#http://localhost:3000#https://$DOMAIN#" $ABSOLUTE_APP_PATH/next/.env

# Replace docker-compose file

cp ./docker-compose.yaml $ABSOLUTE_APP_PATH/docker-compose.yml

# Restart containers
cd $ABSOLUTE_APP_PATH
docker-compose down
docker-compose up -d

